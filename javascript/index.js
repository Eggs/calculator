function evaluateExpression () {
    const expression = document.getElementById('expression').value;
    const regexGetNumbers = /\W/
    const regexGetOperation = /\w/

    const splitExpression = expression.split(regexGetNumbers);
    const splitOperation = expression.split(regexGetOperation).filter(item => item);
    console.log(expression);

    switch (splitOperation[0]) {
        case '+':
            return parseInt(splitExpression[0]) + parseInt(splitExpression[1]);
        case '-':
            return parseInt(splitExpression[0]) - parseInt(splitExpression[1]);
        case '/':
            return parseInt(splitExpression[0]) / parseInt(splitExpression[1]);
        case '*':
            return parseInt(splitExpression[0]) * parseInt(splitExpression[1]);

    }
}

function updateDisplay(updateMode, character) {
    const expression = document.getElementById('expression');

    switch (updateMode) {
        case 'append':
            expression.value += character;
            break;
        case 'overwrite':
            expression.value = character;
            break;
    }
}

function addKeypadEvents () {
    const keypad = document.querySelectorAll('.keypad, .operation');

    keypad.forEach(element => {
        element.addEventListener('click', e => {
            switch (e.target.textContent) {
                case '=':
                    updateDisplay('overwrite', evaluateExpression());
                    break;
                case 'AC':
                    updateDisplay('overwrite', '');
                    break;
                default:
                    updateDisplay('append', e.target.textContent);
                    break;
            }
            
        });
    }); 
}


addKeypadEvents();
